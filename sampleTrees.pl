#!/usr/bin/perl -w

###########################################################################################################
# Program: sampleTrees.pl
# Input: 1)A concatenated file of all the simulated gene trees;
# Output: subsamples of the input
# Usage: perl sampleTrees.pl -in treefile -n [number of trees to sample] -r [number of replicates of n trees] -out [prefix for outfile replicates]
# Author: Barker Lab @ University of Arizona
###########################################################################################################

########
#User Variables
for $i (0..(scalar(@ARGV) - 1))
{
    if ($ARGV[$i] eq "-in")
    {
	$infile = $ARGV[$i + 1];
    }
    if ($ARGV[$i] eq "-n")
    {
	$nSample = $ARGV[$i + 1];
    }
    if ($ARGV[$i] eq "-r")
    {   
	$nReps = $ARGV[$i + 1];
    }
    if ($ARGV[$i] eq "-out")
    {   
	$prefix = $ARGV[$i + 1];
    }
}
########

########
#generate r replicates of n trees by sampling from a set (treefile) WITHOUT replacement
@trees = ();
open FH1, '<', "$infile";
while (<FH1>)
{
    $treeString = $_;
    chomp $treeString;
    push @trees, $treeString;
}
close FH1;

for $i (1..$nReps)
{
    @sampledTrees = ();
    @treeSet = @trees;
    for (1..$nSample)
    {
	$j = int(rand(scalar(@treeSet)));
	push @sampledTrees, $treeSet[$j];
	splice @treeSet, $j, 1;
    }
    open OUT1, '>', "$prefix.$i.tre";
    foreach $tree (@sampledTrees)
    {
	print OUT1 "$tree\n";
    }
    close OUT1;
}
########
exit;